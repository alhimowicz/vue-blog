import axios, { BlogAPI } from '../../api/api.service'

export default {
    state: {
        posts: [],
        post: null,
        comments: [],
        postsCount: 0,
    },
    getters: {
        posts: state => state.posts,
        post: state => state.post,
        comments: state => state.comments,
        postsCount: state => state.postsCount,
    },
    mutations: {
        fetchPostsSuccess: (state, posts) => state.posts = posts,
        fetchPostSuccess: (state, post) => state.post = post,
        fetchCommentsSuccess: (state, comments) => state.comments = comments,
        fetchPostsCountSuccess: (state, postsCount) => state.postsCount = postsCount,
    },
    actions: {
        async fetchPosts({ commit }, payload) {
            try {
                const response = await BlogAPI.fetchPosts(payload?.page, payload?.limit)
                commit('fetchPostsSuccess', response.data)
                commit('fetchPostsCountSuccess', parseInt(response.headers['x-total-count']))
            } catch (error) {
                console.log(error)
            }
        },
        async addPost({ dispatch }, payload) {
            payload.image = await dispatch('convertImageToBase', payload.image)
            await BlogAPI.addPost(payload)
        },
        async fetchPost({ commit }, payload) {
            const response = await axios.get(`/posts/${payload}`)
            commit('fetchPostSuccess', response.data)
        },
        async deletePost(_, payload) {
            await axios.delete(`/posts/${payload}`)
        },
        async updatePost({ commit, dispatch }, payload) {
            payload.image = await dispatch('convertImageToBase', payload.image)
            const response = await axios.put(`/posts/${payload.id}`, {
                id: payload.id,
                image: payload.image,
                title: payload.title,
                body: payload.body
            })
            commit('fetchPostSuccess', response.data)
        },
        async fetchComments({ commit }, payload) {
            const response = await axios.get(`/comments?postId=${payload}`)
            commit('fetchCommentsSuccess', response.data)
        },
        async addComment({ dispatch }, payload) {
            await axios.post('/comments', payload)
            dispatch('fetchComments', payload.postId)
        },
        convertImageToBase(_, file) {
            return new Promise((resolve, reject) => {
                const reader = new FileReader()
                if (!file) resolve('')
                reader.readAsDataURL(file)
                reader.onload = () => resolve(reader.result)
                reader.onerror = error => reject(error) 
            })
        },
        async convertBaseToFile(_, dataUrl) {
            const blob = await (await fetch(dataUrl)).blob()
            return new File([blob], 'image', { type: blob.type })
        }
    }
}