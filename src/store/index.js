import Vue from 'vue'
import Vuex from 'vuex'
import Blog from './modules/blog'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        loadingStatus: false
    },
    getters: {
        loadingStatus: state => state.loadingStatus
    },
    mutations: {
        loadingStart: state => state.loadingStatus = true,
        loadingEnd: state => state.loadingStatus = false
    },
    modules: {
        Blog
    }
})