import store from '../store/index'

const Loader = {
    loadingStart: () => store.commit('loadingStart', null, { root: true }),
    loadingEnd: () => store.commit('loadingEnd', null, { root: true })
}

export default Loader
