import axios from 'axios'

const instance = axios.create({
    baseURL: 'http://localhost:3000',
    withCredentials: true
})

export default instance

export const BlogAPI = {
    fetchPosts(page = 1, limit = 5) {
        return instance.get('/posts', {
            params: {
                _limit: limit,
                _page: page
            }
        })
    },

    addPost({ id, image, title, body }) {
        return instance.post('/posts', {
            id,
            image,
            title,
            body
        })
    }
}
